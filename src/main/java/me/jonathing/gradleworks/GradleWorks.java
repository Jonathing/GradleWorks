package me.jonathing.gradleworks;

import me.jonathing.gradleworks.dependency.ForgeDependency;
import me.jonathing.gradleworks.dependency.MixinDependency;
import me.jonathing.gradleworks.dependency.ModDependency;
import me.jonathing.gradleworks.info.dependency.MixinInfo;
import me.jonathing.gradleworks.info.publish.PublicationInfo;
import me.jonathing.gradleworks.info.publish.RepositoryInfo;
import me.jonathing.gradleworks.util.AssertUtil;
import me.jonathing.gradleworks.util.FileIOUtil;
import me.jonathing.gradleworks.util.GradleUtil;
import me.jonathing.gradleworks.util.Triple;
import net.minecraftforge.gradle.common.util.ModConfig;
import net.minecraftforge.gradle.common.util.RunConfig;
import net.minecraftforge.gradle.mcp.tasks.GenerateSRG;
import net.minecraftforge.gradle.userdev.DependencyManagementExtension;
import net.minecraftforge.gradle.userdev.UserDevExtension;
import net.minecraftforge.gradle.userdev.UserDevPlugin;
import net.minecraftforge.gradle.userdev.jarjar.JarJarProjectExtension;
import net.minecraftforge.gradle.userdev.tasks.JarJar;
import net.minecraftforge.gradle.userdev.tasks.RenameJarInPlace;
import org.apache.tools.ant.taskdefs.condition.Os;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.gradle.api.*;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.ModuleDependency;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;
import org.gradle.api.logging.Logger;
import org.gradle.api.plugins.ExtraPropertiesExtension;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.MavenPublication;
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.compile.JavaCompile;
import org.gradle.api.tasks.javadoc.Javadoc;
import org.gradle.api.tasks.testing.Test;
import org.gradle.jvm.toolchain.JavaLanguageVersion;
import org.gradle.plugins.ide.eclipse.EclipsePlugin;
import org.jetbrains.annotations.Nullable;
import org.moddingx.modgradle.plugins.javadoc.JavadocPlugin;
import org.moddingx.modgradle.plugins.mapping.MappingPlugin;
import org.moddingx.modgradle.plugins.sourcejar.MergeJarWithSourcesTask;
import org.moddingx.modgradle.plugins.sourcejar.SourceJarPlugin;
import org.parchmentmc.librarian.forgegradle.LibrarianForgeGradlePlugin;
import org.spongepowered.asm.gradle.plugins.MixinExtension;
import org.spongepowered.asm.gradle.plugins.MixinGradlePlugin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static me.jonathing.gradleworks.reader.YAMLParser.parseDependencies;
import static me.jonathing.gradleworks.reader.YAMLParser.parseRepositories;

@SuppressWarnings("unused")
public class GradleWorks implements Plugin<Project>
{
    public static final String NAME = "GradleWorks";
    private static String VERSION = "UNKNOWN";
    public static final String AUTHOR = "Jonathing";
    public static final String PLATFORM = "ForgeGradle 5";
    public static final String EXT_KEY = NAME.toLowerCase();

    private GradleWorksExtension properties;
    private Project project;

    private boolean hasJarJarDeps = false;

    @Override
    public void apply(Project project)
    {
        // setup gradleworks
        this.project = project;
        this.project.getBuildscript().getConfigurations().getByName("classpath").getDependencies().forEach(dependency ->
        {
            if ("me.jonathing".equalsIgnoreCase(dependency.getGroup()) && "gradleworks".equalsIgnoreCase(dependency.getName()))
                VERSION = dependency.getVersion();
        });

        this.project.getLogger().lifecycle("[GradleWorks] Starting GradleWorks version {}", VERSION);

        // Initialize GradleWorks and related plugins
        // We're going to have to trust that GradleWorks MDK buildscript is untouched.
        this.project.getLogger().info("[GradleWorks] Creating GradleWorks project extension");
        this.properties = this.project.getExtensions().create(EXT_KEY, GradleWorksExtension.class, this.project);
        this.applyPlugin(UserDevPlugin.class, "MinecraftForge ForgeGradle");
        this.applyPlugin(MixinGradlePlugin.class, "SpongePowered MixinGradle", this.properties.getMixinInfo().enabled());
        this.applyPlugin(LibrarianForgeGradlePlugin.class, "ParchmentMC Librarian");
        this.applyPlugin(MappingPlugin.class, "ModdingX ModGradle Mapping Channels");
        this.applyPlugin(EclipsePlugin.class, "Eclipse");
        this.applyPlugin(MavenPublishPlugin.class, "Maven Publishing", this.properties.isPublishingEnabled());

        // Check that all necessary extensions for GradleWorks initialization were loaded successfully
        @Nullable var publishing = this.properties.isPublishingEnabled() ? this.getExtension(PublishingExtension.class, PublishingExtension.NAME) : null;
        var minecraft = this.getExtension(UserDevExtension.class, UserDevExtension.EXTENSION_NAME);
        @Nullable var mixin = this.properties.getMixinInfo().enabled() ? this.getExtension(MixinExtension.class, "mixin") : null;
        var ext = this.getExtension(ExtraPropertiesExtension.class, ExtraPropertiesExtension.EXTENSION_NAME);
        var java = this.getExtension(JavaPluginExtension.class, "java");
        var jarJar = this.getExtension(JarJarProjectExtension.class, JarJarProjectExtension.EXTENSION_NAME);
        var reobf = (NamedDomainObjectContainer<RenameJarInPlace>) this.getExtension("reobf");

        // set project properties
        this.project.getLogger().info("[GradleWorks] Configuring project");
        this.project.setVersion(this.properties.getModInfo().version());
        this.project.setGroup(this.properties.getModInfo().group());
        this.project.setProperty("archivesBaseName", this.properties.getModInfo().archivesBaseName(this.properties.getMcInfo().version()));

        // set extra properties
        ext.set("reobfFile", GradleUtil.getFile(this.project.getBuildDir(), "libs", String.format("%s-%s.jar", this.project.getProperties().get("archivesBaseName"), this.project.getVersion())));

        // set java language level
        java.getToolchain().getLanguageVersion().set(getJavaVersion(this.project.getLogger(), this.properties.getJavaVersion(), this.properties.getMcInfo().version()));

        // generic gradle setup
        this.setupTasks();
        if (this.properties.isPublishingEnabled())
            this.handlePublishing();
        this.handleRepositories();
        this.handleDependencies();

        // forgegradle setup
        this.handleMinecraft();

        // mixin setup
        if (this.properties.getMixinInfo().enabled())
            this.handleMixin();

        this.project.getLogger().info("[GradleWorks] Adding data-genned resources to project resources");
        java.getSourceSets().named(SourceSet.MAIN_SOURCE_SET_NAME).get().resources(resources ->
                resources.srcDir(GradleUtil.getFile(this.project.getProjectDir(), "src", "generated", "resources")));

        this.project.getLogger().info("[GradleWorks] Configuring built Jar Manifest");
        this.project.getTasks().getByName("jar", task ->
        {
            Jar jar = (Jar) task;
            jar.manifest(manifest ->
            {
                LinkedHashMap<String, Object> attributes = new LinkedHashMap<>();
                attributes.put("Specification-Title", this.properties.getModInfo().modId());
                attributes.put("Specification-Vendor", this.properties.getModInfo().author());
                attributes.put("Specification-Version", String.valueOf(1));
                attributes.put("Implementation-Title", this.project.getName());
                attributes.put("Implementation-Version", this.project.getVersion());
                attributes.put("Implementation-Vendor", this.properties.getModInfo().author());
                attributes.put("Implementation-Timestamp", SimpleDateFormat.getDateInstance().format(new Date()));
                if (this.properties.getMixinInfo().connector() != null)
                    attributes.put("MixinConnector", this.properties.getMixinInfo().connector());
                manifest.attributes(attributes);
            });
            jar.finalizedBy("reobfJar");
        });

        // Enforce UTF-8 for compilation. Source: ModUtils by ModdingX
        this.project.getLogger().info("[GradleWorks] Setting UTF-8 as the default encoding");
        this.project.afterEvaluate(p ->
        {
            p.getTasks().withType(JavaCompile.class).configureEach(compiler -> compiler.getOptions().setEncoding("UTF-8"));
            p.getTasks().withType(Test.class).configureEach(tests -> tests.setDefaultCharacterEncoding("UTF-8"));
            p.getTasks().withType(Javadoc.class).configureEach(javadoc -> javadoc.getOptions().setEncoding("UTF-8"));
        });

        this.handleJarJar();

        // Prevent Mixin annotation processor from fucking with IDEA's annotation processor settings. Source: ModUtils by ModdingX
        this.project.getLogger().info("[GradleWorks] Enforcing the default annotation processor path to the root directory");
        if (this.properties.getMixinInfo().enabled() && Boolean.getBoolean("idea.sync.active"))
        {
            this.project.afterEvaluate(p ->
                    p.getTasks().withType(JavaCompile.class).all(all -> all.getOptions().setAnnotationProcessorPath(p.files())));
        }

        this.project.getLogger().lifecycle("[GradleWorks] Initial setup complete\n");

        this.printInfo();
    }

    public static GradleWorksExtension getProperties(Project project)
    {
        return project.getExtensions().findByType(GradleWorksExtension.class);
    }

    private void applyPlugin(Class<? extends Plugin<Project>> plugin, String name)
    {
        applyPlugin(plugin, name, true);
    }

    private <T> T getExtension(Class<T> extension, String name)
    {
        return Objects.requireNonNull(this.project.getExtensions().findByType(extension), () -> this.alertForMissingExt(name));
    }

    private Object getExtension(String extension)
    {
        return Objects.requireNonNull(this.project.getExtensions().findByName(extension), () -> this.alertForMissingExt(extension));
    }

    private void applyPlugin(Class<? extends Plugin<Project>> plugin, String name, boolean toggle)
    {
        try
        {
            this.project.apply(apply ->
            {
                if (!toggle) return;

                this.project.getLogger().info("[GradleWorks] Applying plugin {}", name);
                apply.plugin(plugin);
            });
        }
        catch (Throwable t)
        {
            this.project.getLogger().error("ERROR: [GradleWorks] Failed to apply plugin {}! Did you modify the build.gradle file?", name);
            throw t;
        }
    }

    private String alertForMissingExt(String name)
    {
        this.project.getLogger().error("ERROR: [GradleWorks] Failed to load extension {}! Did you modify the build.gradle file?", name);
        return String.format("GradleWorks detected that a required extension \"%s\" failed to load.", name);
    }

    private void setupTasks()
    {
        JavaPluginExtension java = this.project.getExtensions().findByType(JavaPluginExtension.class);

        // configure tasks
        final TaskProvider<Jar> deobfJar = this.project.getTasks().register("deobfJar", Jar.class);
        final TaskProvider<Jar> javadocJar = this.project.getTasks().register("javadocJar", Jar.class);

        // setup sourcesJar plugin from ModGradle
        this.applyPlugin(SourceJarPlugin.class, "ModdingX ModGradle SourceJar");

        this.project.getLogger().info("[GradleWorks] Creating deobfuscated (mapped) Jar task in deobfJar");
        deobfJar.configure(task ->
        {
            task.dependsOn(this.project.getTasks().named("classes"));
            task.getArchiveClassifier().set("deobf");

            task.from(java.getSourceSets().named(SourceSet.MAIN_SOURCE_SET_NAME).get().getJava().getSourceDirectories());
            task.from(java.getSourceSets().named(SourceSet.MAIN_SOURCE_SET_NAME).get().getOutput());
        });

        this.project.getLogger().info("[GradleWorks] Creating Javadoc jar task in javadocJar");
        javadocJar.configure(task ->
        {
            task.dependsOn(this.project.getTasks().named("classes"));
            task.getArchiveClassifier().set("javadoc");

            task.from(this.project.getTasks().named("javadoc"));
        });

        this.applyPlugin(JavadocPlugin.class, "ModdingX ModGradle Javadoc Linking");
    }

    private void handlePublishing()
    {
        ExtraPropertiesExtension ext = this.project.getExtensions().findByType(ExtraPropertiesExtension.class);
        PublishingExtension publishing = this.project.getExtensions().findByType(PublishingExtension.class);

        this.project.getTasks().named("publish").configure(t -> t.dependsOn(this.project.getTasks().named("build")));

        // setup publishing (if applicable)
        PublicationInfo publicationInfo = this.properties.getPublicationInfo();
        publishing.getPublications().create("mavenJava", MavenPublication.class, publication ->
        {
            this.project.getLogger().info("[GradleWorks] Creating maven publication for publishing");

            publication.setGroupId(publicationInfo.getGroup());
            publication.setArtifactId(publicationInfo.getArtifactId());
            publication.setVersion(publicationInfo.getVersion());

            var artifacts = new LinkedList<>();
            artifacts.add(ext.get("reobfFile"));
            artifacts.add(this.project.getTasks().named("sourceJar"));
            if (publicationInfo.shouldPublishJavadoc()) artifacts.add(this.project.getTasks().named("javadocJar"));
            publication.setArtifacts(artifacts);
        });

        var data = FileIOUtil.getYAMLData(new File(this.project.getProjectDir(), "publishing.yaml"));
        List<RepositoryInfo> repositories = parseRepositories(this.project, data.readObject("repositories", false));
        repositories.forEach(repo ->
        {
            if (repo.getUrl() == null || repo.getUrl().equals("null")) return;

            try
            {
                publishing.getRepositories().maven(maven ->
                {
                    this.project.getLogger().info("[GradleWorks] Adding maven publishing repository {} with URL {}", repo.getName(), repo.getUrl());

                    maven.setName(repo.getName());
                    maven.setUrl(repo.getUrl());
                    if (repo.requiresAuthentication())
                    {
                        maven.credentials(credentials ->
                        {
                            Object username = this.project.findProperty(repo.getUsernameVars().getLeft());
                            username = username != null ? username : System.getenv(repo.getUsernameVars().getRight());
                            Object password = this.project.findProperty(repo.getPasswordVars().getLeft());
                            password = password != null ? password : System.getenv(repo.getPasswordVars().getRight());

                            credentials.setUsername(AssertUtil.stringRequireNonNull(username, "username", true));
                            credentials.setPassword(AssertUtil.stringRequireNonNull(password, "password", true));
                        });
                    }
                });
            }
            catch (NullPointerException e)
            {
                this.project.getLogger().error(String.format("ERROR: [GradleWorks] Failed to add a publishing repository with URL '%s'", repo.getUrl()), e);
            }
        });
    }

    private void handleRepositories()
    {
        var data = FileIOUtil.getYAMLData(new File(this.project.getProjectDir(), "dependencies.yaml"));
        RepositoryHandler repoHandler = this.project.getRepositories();
        List<ModDependency> deps = parseDependencies(this.project, data.readObject("dependencies", false));

        this.project.getLogger().info("[GradleWorks] Adding Maven Central to project repositories.");
        repoHandler.mavenCentral();

        this.project.getLogger().info("[GradleWorks] Adding JitPack to project repositories.");
        repoHandler.maven(repo -> repo.setUrl("https://jitpack.io"));

        this.project.getLogger().info("[GradleWorks] Adding CurseMaven to project repositories.");
        repoHandler.maven(repo ->
        {
            repo.setUrl("https://www.cursemaven.com");
            repo.mavenContent(filter -> filter.includeGroup("curse.maven"));
        });

        deps.stream()
                .map(ModDependency::getMavenURL)
                .filter(Objects::nonNull)
                .distinct()
                .filter(url -> !repoHandlerContains(() -> repoHandler, url))
                .forEach(url ->
                        {
                            if (url.equalsIgnoreCase("LOCAL"))
                            {
                                this.project.getLogger().info("[GradleWorks] Adding local maven to project repositories.");
                                repoHandler.mavenLocal();
                            }

                            this.project.getLogger().info("[GradleWorks] Adding maven repository with URL {} to project repositories.", url);
                            repoHandler.maven(repo -> repo.setUrl(url));
                        }
                );
    }

    private void handleDependencies()
    {
        DependencyManagementExtension fg = this.project.getExtensions().findByType(DependencyManagementExtension.class);
        JarJarProjectExtension jarJar = this.project.getExtensions().findByType(JarJarProjectExtension.class);
        DependencyHandler dependencies = this.project.getDependencies();
        List<Triple<String, String, Dependency>> parsedDeps = new ArrayList<>();

        this.project.getConfigurations().getByName("implementation",
                implementation -> implementation.extendsFrom(this.project.getConfigurations().maybeCreate("library")));

        for (ModDependency dep : this.getDependencies(this.project))
        {
            Dependency parsedDep = dependencies.create(dep.getLocation().toString());
            if (dep.isDeobf())
                parsedDep = fg.deobf(parsedDep);

            parsedDeps.add(new Triple<>(dep.getType(), dep.getLocation().toString(), parsedDep));
        }

        parsedDeps.forEach(dependency ->
        {
            String[] options = dependency.a.split(",", 4);
            String group = options[0];
            String transitive = options.length > 1 ? options[1].toLowerCase() : "default";

            this.project.getLogger().info(
                    "[GradleWorks] Parsing {} \"{}\"",
                    switch (group)
                            {
                                case "minecraft" -> "Minecraft dependency";
                                case "annotationProcessor" -> "annotation processor";
                                case "runtimeOnly" -> "runtime dependency";
                                case "compileOnly" -> "compile-only dependency";
                                case "library" -> "non-mod library";
                                case "jarJar" -> "Jar-in-Jar artifact";
                                default -> "dependency";
                            },
                    dependency.b
            );

            Dependency newDep = dependencies.add(group, dependency.c);
            if (newDep instanceof ModuleDependency && !transitive.equals("default"))
            {
                ((ModuleDependency) newDep).setTransitive(Boolean.parseBoolean(transitive));
            }

            if (group.equals("jarJar"))
            {
                if (!this.hasJarJarDeps) this.hasJarJarDeps = true;

                String pin = options[2];
                String ranged = options[3];

                jarJar.ranged(newDep, ranged);
                jarJar.pin(newDep, pin);
            }
        });
    }

    private void handleJarJar()
    {
        if (this.project.getTasks().withType(JarJar.class).size() > 1)
        {
            this.project.getLogger().warn("WARNING: [GradleWorks] More than one Jar-in-Jar task was found! GradleWorks is not designed for super-complex or multi-project workspaces in mind! If you believe this is in error, please make an issue on the issue tracker!");
        }

        var jarJar = this.project.getTasks().named("jarJar");
        if (!jarJar.isPresent()) return;

        if (!this.hasJarJarDeps)
        {
            this.project.getLogger().info("[GradleWorks] No Jar-in-Jar dependencies have been declared. Skipping jarJar task");
            return;
        }

        this.project.getLogger().info("[GradleWorks] Configuring the jarJar task for Jar-in-Jar compatibility");
        this.project.getTasks().named("build").configure(t -> t.dependsOn(jarJar));

        try {
            this.project.getTasks().named("reobfJarJar").get();
        } catch (UnknownTaskException e) {
            //noinspection unchecked
            ((NamedDomainObjectContainer<RenameJarInPlace>) this.project.getExtensions().getByName("reobf")).create("jarJar");
        }
        final RenameJarInPlace reobfJarJar = (RenameJarInPlace) this.project.getTasks().named("reobfJarJar").get();;
        jarJar.configure(t -> {
            ((JarJar) t).getArchiveClassifier().convention((String) null);
            ((JarJar) t).getArchiveClassifier().set((String) null);
            t.finalizedBy(reobfJarJar);
        });
        this.project.getTasks().named("jar").configure(t -> ((Jar) t).getArchiveClassifier().set("no-embeds"));
    }

    private void handleMinecraft()
    {
        UserDevExtension minecraft = this.project.getExtensions().findByType(UserDevExtension.class);
        JavaPluginExtension java = this.project.getExtensions().findByType(JavaPluginExtension.class);

        //noinspection ConstantConditions
        minecraft.mappings(this.properties.getMcInfo().mappingsChannel(), this.properties.getMcInfo().mappingsVersion());

        File accessTransformer = GradleUtil.getFile(this.project.getProjectDir(), "src", "main", "resources", "META-INF", "accesstransformer.cfg");
        if (accessTransformer.exists())
        {
            this.project.getLogger().info("[GradleWorks] Adding access transformer config to ForgeGradle");
            minecraft.setAccessTransformer(accessTransformer);
        }

        NamedDomainObjectContainer<RunConfig> runs = minecraft.getRuns();
        this.project.getLogger().info("[GradleWorks] Creating client run config");
        runs.create("client", client ->
                client.property("forge.enabledGameTestNamespaces", this.properties.getModInfo().modId()));
        this.project.getLogger().info("[GradleWorks] Creating server run config");
        runs.create("server", server ->
        {
            server.args("nogui");
            server.property("forge.enabledGameTestNamespaces", this.properties.getModInfo().modId());
        });
        this.project.getLogger().info("[GradleWorks] Creating GameTest server run config");
        runs.create("gameTestServer", gameTestServer ->
                gameTestServer.property("forge.enabledGameTestNamespaces", this.properties.getModInfo().modId()));
        this.project.getLogger().info("[GradleWorks] Creating data generation run config");
        runs.create("data", data ->
        {
            data.args(
                    "--mod", this.properties.getModInfo().modId(),
                    "--all",
                    "--output", GradleUtil.getFile(this.project.getProjectDir(), "src", "generated", "resources"),
                    "--existing", GradleUtil.getFile(this.project.getProjectDir(), "src", "main", "resources")
            );

            data.property("%s.datagen".formatted(this.properties.getModInfo().modId()), "true");

            if (Os.isFamily(Os.FAMILY_MAC))
                data.jvmArg("-XstartOnFirstThread");
        });

        this.properties.getRunsInfo().forEach(pair ->
        {
            RunConfig temp;
            try
            {
                temp = runs.getByName(pair.getLeft());
            }
            catch (UnknownDomainObjectException e)
            {
                temp = runs.create(pair.getLeft());
            }
            RunConfig run = temp;

            pair.getRight().forEach(property -> run.property(property.getLeft(), property.getRight()));
        });

        this.project.getLogger().info("[GradleWorks] Configuring base run config");
        runs.all(all ->
        {
            all.lazyToken(
                    "minecraft_classpath",
                    () -> DefaultGroovyMethods.join(
                            (Iterable<?>) this.project.getConfigurations().getByName("library").copyRecursive().resolve().stream().map(File::getAbsoluteFile).collect(Collectors.toList()),
                            File.pathSeparator)
            );

            all.workingDirectory(GradleUtil.getFile(this.project.getProjectDir(), "run"));

            if (!this.properties.getMixinInfo().enabled())
            {
                File srgToMcpFile = ((GenerateSRG) this.project.getTasks().getByName("createSrgToMcp")).getOutputs().getFiles().getFiles().stream().findFirst().orElseThrow(GradleWorksException::new);
                all.property("net.minecraftforge.gradle.GradleStart.srg.srg-mcp", srgToMcpFile);
                all.property("mixin.env.remapRefMap", String.valueOf(true));
                all.property("mixin.env.refMapRemappingFile", srgToMcpFile);
            }

            all.property("%s.iside".formatted(this.properties.getModInfo().modId()), "true");

            this.properties.getRunsInfo().getGlobalProperties()
                    .forEach(pair -> all.property(pair.getLeft(), pair.getRight()));

            ModConfig modConfig = new ModConfig(this.project, this.properties.getModInfo().modId());
            modConfig.source(java.getSourceSets().named(SourceSet.MAIN_SOURCE_SET_NAME).get());
            all.getMods().add(modConfig);
        });
    }

    private void handleMixin()
    {
        String modId = this.properties.getModInfo().modId();
        MixinExtension mixin = this.project.getExtensions().findByType(MixinExtension.class);
        JavaPluginExtension java = this.project.getExtensions().findByType(JavaPluginExtension.class);

        this.project.getLogger().info("[GradleWorks] Configuring MixinGradle");
        mixin.add(
                java.getSourceSets().named(SourceSet.MAIN_SOURCE_SET_NAME).get(),
                String.format("%s.refmap.json", modId)
        );
        if (this.properties.getMixinInfo().connector() == null)
            mixin.config(String.format("%s.mixins.json", modId));
    }

    private static boolean repoHandlerContains(Supplier<RepositoryHandler> repoHandler, String url)
    {
        final boolean[] result = {false};
        try
        {
            repoHandler.get().forEach(repo ->
            {
                if (repo instanceof MavenArtifactRepository)
                {
                    if (((MavenArtifactRepository) repo).getUrl().toString().equals(url))
                    {
                        result[0] = true;
                        throw new GradleWorksException();
                    }
                }
            });
        }
        catch (GradleWorksException ignored)
        {
        }

        return result[0];
    }

    private Iterable<ModDependency> getDependencies(Project project)
    {
        var data = FileIOUtil.getYAMLData(new File(project.getProjectDir(), "dependencies.yaml"));
        List<ModDependency> list = new ArrayList<>();

        ForgeDependency forge = new ForgeDependency(
                this.properties.getMcInfo(),
                this.properties.getForgeInfo()
        );
        MixinDependency mixin = new MixinDependency(
                MixinInfo.of(data.readData("mixin"))
        );

        if (forge.isEnabled()) list.add(forge);
        list.addAll(parseDependencies(project, data.readObject("dependencies", false)));
        if (mixin.isEnabled()) list.add(mixin);

        return list;
    }

    private static JavaLanguageVersion getJavaVersion(Logger log, String s, String mcVersion)
    {
        if (s.equalsIgnoreCase("AUTO_DETECT"))
        {
            try
            {
                String[] split = mcVersion.split("\\.");
                int version = 17;

                // check major version
                if (!split[0].equals("1"))
                    throw new IllegalArgumentException("Minecraft major version shouldn't be anything other than '1'!");

                if (Integer.parseInt(split[1]) <= 16)
                    version = 8;
                else if (Integer.parseInt(split[1]) == 17)
                    version = 16;

                log.info("[GradleWorks] Auto-selected Java {} for Minecraft {}", version, mcVersion);
                return JavaLanguageVersion.of(version);
            }
            catch (Throwable t)
            {
                log.warn("WARNING: [GradleWorks] Failed to auto-detect which Java version to use for Minecraft %s! Using default value of 17.".formatted(mcVersion), t);
                return JavaLanguageVersion.of(17);
            }
        }

        try
        {
            log.info("[GradleWorks] Using Java {} as specified in workspace.yaml", s);
            return JavaLanguageVersion.of(s);
        }
        catch (NumberFormatException e)
        {
            log.warn(String.format("WARNING: [GradleWorks] Found an invalid Java version of %s in your workspace.yaml file. Using default value of 17.", s), e);
            return JavaLanguageVersion.of(17);
        }
    }

    private void printInfo()
    {
        String info = "%s for %s%n".formatted(NAME, PLATFORM) +
                "Written by %s%n".formatted(AUTHOR) +
                "Version %s%n".formatted(VERSION) +
                "\nJava Information\n" +
                "- Java: %s%n".formatted(System.getProperty("java.version")) +
                "- JVM: %s (%s)%n".formatted(System.getProperty("java.vm.version"), System.getProperty("java.vendor")) +
                "- Arch: %s%n".formatted(System.getProperty("os.arch")) +
                "\nMinecraft Information\n" +
                "- Minecraft: %s%n".formatted(this.properties.getMcInfo().version()) +
                "- Minecraft Forge: %s%n".formatted(this.properties.getForgeInfo().version()) +
                "- Mappings: %s %s%n".formatted(this.properties.getMcInfo().mappingsVersion(), this.properties.getMcInfo().mappingsChannel()) +
                "\nMod Information\n" +
                "- Mod ID: %s%n".formatted(this.properties.getModInfo().modId()) +
                "- Name: %s%n".formatted(this.properties.getModInfo().name()) +
                "- Version: %s%n".formatted(this.properties.getModInfo().version());

        this.project.getLogger().lifecycle("");
        this.project.getLogger().lifecycle(info);
        this.project.getLogger().lifecycle("");
    }

    public static String getVersion()
    {
        return VERSION;
    }
}
