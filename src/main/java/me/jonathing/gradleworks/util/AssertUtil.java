package me.jonathing.gradleworks.util;

public final class AssertUtil
{
    public static String stringRequireNonNull(String s, String name, boolean throwIfNull)
    {
        if (s == null || s.equals("null") || s.isEmpty())
        {
            if (throwIfNull)
                throw new NullPointerException(String.format("Required value '%s' cannot be null or empty!", name));
            return null;
        }

        return s;
    }

    public static String stringRequireNonNull(Object obj, String name, boolean throwIfNull)
    {
        return stringRequireNonNull(String.valueOf(obj), name, throwIfNull);
    }
}
