package me.jonathing.gradleworks.util;

import me.jonathing.gradleworks.GradleWorksException;
import me.jonathing.gradleworks.reader.MappedDataReader;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public final class FileIOUtil
{
    public static MappedDataReader getYAMLData(File file)
    {
        try
        {
            return MappedDataReader.of(new Yaml().load(new FileInputStream(file)));
        }
        catch (FileNotFoundException e)
        {
            throw new GradleWorksException(String.format("The '%s' file doesn't exist!", file.getName()), e);
        }
    }
}
