package me.jonathing.gradleworks.util;

public class Triple<A, B, C> {
    public final A a;
    public final B b;
    public final C c;

    public Triple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof Triple)) {
            return false;
        } else {
            Triple<?, ?, ?> other = (Triple)obj;
            return this.a.equals(other.a) && this.b.equals(other.b) && this.c.equals(other.c);
        }
    }

    public String toString() {
        return String.format("(%s, %s, %s)", this.a, this.b, this.c);
    }
}
