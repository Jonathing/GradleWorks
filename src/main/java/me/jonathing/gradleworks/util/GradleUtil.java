package me.jonathing.gradleworks.util;

import me.jonathing.gradleworks.GradleWorks;
import org.gradle.api.Project;

import java.io.File;

public final class GradleUtil
{
    @SuppressWarnings("ReassignedVariable")
    public static File getFile(File parent, String... paths)
    {
        File result = parent;
        for (String path : paths)
        {
            result = new File(result, path);
        }

        return result;
    }

    public static boolean isMixinEnabled(Project project)
    {
        return GradleWorks.getProperties(project).getMixinInfo().enabled();
    }
}
