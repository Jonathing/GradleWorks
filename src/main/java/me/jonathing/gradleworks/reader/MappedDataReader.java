package me.jonathing.gradleworks.reader;

import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

public class MappedDataReader implements IDataReader.IObjectDataReader<Map<String, ?>, MappedDataReader>
{
    private final Map<String, ?> data;

    @SuppressWarnings("unchecked")
    @Nullable
    public static MappedDataReader of(Object data)
    {
        return data != null ? new MappedDataReader((Map<String, ?>) data) : null;
    }

    private MappedDataReader(Map<String, ?> data)
    {
        this.data = data;
    }

    @Override
    public int size()
    {
        return this.data.size();
    }

    @Override
    public Map<String, ?> getRawData()
    {
        return this.data;
    }

    @Override
    @Nullable
    public MappedDataReader readData(String name)
    {
        var reader = MappedDataReader.of(this.data.get(name));
        return reader.data != null ? reader : null;
    }

    @Override
    public boolean containsKey(String name)
    {
        return this.data.containsKey(name);
    }

    @Override
    public Object readObject(String name, boolean requireNotNull)
    {
        BiFunction<Object, String, Object> nullCheck = requireNotNull
                ? (o, s) -> Objects.requireNonNull(o, "Required value '%s' cannot be null!".formatted(s))
                : (o, s) -> o;
        return nullCheck.apply(this.data.get(name), name);
    }
}
