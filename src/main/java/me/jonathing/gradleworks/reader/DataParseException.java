package me.jonathing.gradleworks.reader;

import me.jonathing.gradleworks.GradleWorksException;

public class DataParseException extends GradleWorksException
{
    public DataParseException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
