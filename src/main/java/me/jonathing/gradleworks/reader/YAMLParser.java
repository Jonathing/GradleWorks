package me.jonathing.gradleworks.reader;

import me.jonathing.gradleworks.GradleWorksException;
import me.jonathing.gradleworks.dependency.JarJarDependency;
import me.jonathing.gradleworks.dependency.ModDependency;
import me.jonathing.gradleworks.info.dependency.JarJarInfo;
import me.jonathing.gradleworks.info.publish.RepositoryInfo;
import org.gradle.api.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class YAMLParser
{
    @SuppressWarnings("unchecked")
    public static List<ModDependency> parseDependencies(Project project, Object obj)
    {
        if (obj != null)
        {
            List<Map<String, ?>> data = (List<Map<String, ?>>) obj;
            List<ModDependency> list = new ArrayList<>(data.size());
            data.forEach(dependency ->
            {
                ModDependency.Location location = ModDependency.Location.invalid();
                try
                {
                    boolean enabled;
                    String type = String.valueOf(dependency.get("type"));
                    boolean deobf;
                    String repoURL = String.valueOf(dependency.get("repoURL"));
                    String transitive = String.valueOf(dependency.get("transitive"));
                    JarJarInfo jarJar;

                    try
                    {
                        location = ModDependency.Location.valueOf(dependency.get("location"));
                        if (location.isInvalid())
                            throw new GradleWorksException("A parsed location was declared as invalid during YAML parsing!");
                    }
                    catch (NullPointerException | GradleWorksException e)
                    {
                        throw new DataParseException("Required value 'location' is not a valid location!", e);
                    }

                    if (type.equals("null"))
                    {
                        project.getLogger().warn("WARNING: [GradleWorks] No type specified for dependency '{}'. Will use the default of \"both\". It is advised to add the type for each dependency, even if they are the same.", location);
                        type = "both";
                    }

                    if (repoURL.equals("null") && !location.isCurseforge())
                    {
                        project.getLogger().warn("WARNING: [GradleWorks] No repository URL specified for dependency '{}'. Will attempt to download from repositories added by other dependencies and the default repositories. It is advised to add the repoURL for each dependency, even if they are the same.", location);
                        repoURL = null;
                    }

                    if (transitive.equals("null"))
                    {
                        project.getLogger().info("[GradleWorks] No transitive preference specified for dependency '{}'. Will use the default for the dependency.", location);
                    }

                    String currentValue = "enabled";
                    try
                    {
                        enabled = (Boolean) dependency.get("enabled");
                        currentValue = "deobf";
                        deobf = (Boolean) dependency.get("deobf");
                    }
                    catch (ClassCastException e)
                    {
                        throw new DataParseException("Required value '%s' is not a valid boolean!".formatted(currentValue), e);
                    }

                    try
                    {
                        jarJar = JarJarInfo.of(MappedDataReader.of(dependency.get("jarJar")));
                    }
                    catch (Throwable t)
                    {
                        project.getLogger().error("ERROR: [GradleWorks] Failed to parse JarJar information. Skipping for now, but marked as an error since it might be important.", new DataParseException("JarJar configuration for dependency '%s' in 'dependencies.yaml' is invalid or incomplete!".formatted(location), t));
                        jarJar = null;
                    }

                    ModDependency parsedDep = new ModDependency(enabled, type, deobf, location, repoURL, transitive);
                    if (parsedDep.isEnabled())
                    {
                        list.add(parsedDep);
                        if (jarJar != null && jarJar.enabled())
                        {
                            list.add(new JarJarDependency(parsedDep, jarJar.ranged(), jarJar.pin()));
                        }
                    }
                }
                catch (Throwable t)
                {
                    project.getLogger().error("ERROR: [GradleWorks] Failed to add dependency with location '%s'".formatted(location), t);
                }
            });

            return list;
        }

        return Collections.emptyList();
    }

    public static List<RepositoryInfo> parseRepositories(Project project, Object obj)
    {
        if (obj != null)
        {
            List<Map<String, ?>> data = (List<Map<String, ?>>) obj;
            List<RepositoryInfo> list = new ArrayList<>(data.size());
            data.forEach(o ->
            {
                try
                {
                    list.add(RepositoryInfo.of(project, MappedDataReader.of(o)));
                }
                catch (DataParseException e)
                {
                    project.getLogger().error("ERROR: [GradleWorks] Failed to add repository. See log/stacktrace for more information.", e);
                }
            });

            return list;
        }

        return Collections.emptyList();
    }
}
