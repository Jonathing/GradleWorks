package me.jonathing.gradleworks.reader;

import me.jonathing.gradleworks.util.AssertUtil;
import org.jetbrains.annotations.Nullable;

public interface IDataReader
{
    boolean containsKey(String name);

    Object readObject(String name, boolean requireNotNull);

    default String readString(String name, boolean requireNotNull)
    {
        return AssertUtil.stringRequireNonNull(this.readObject(name, false), name, requireNotNull);
    }

    default boolean readBoolean(String name, boolean requireNotNull)
    {
        return (Boolean) this.readObject(name, requireNotNull);
    }

    interface IObjectDataReader<T, R extends IObjectDataReader<T, R>> extends IDataReader
    {
        int size();

        T getRawData();

        @Nullable
        R readData(String name);
    }
}
