package me.jonathing.gradleworks;

import org.gradle.api.GradleException;

public class GradleWorksException extends GradleException
{
    public GradleWorksException()
    {
    }

    public GradleWorksException(String message)
    {
        super(message);
    }

    public GradleWorksException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
