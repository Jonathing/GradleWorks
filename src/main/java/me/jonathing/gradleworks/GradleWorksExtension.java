package me.jonathing.gradleworks;

import me.jonathing.gradleworks.info.dependency.ForgeInfo;
import me.jonathing.gradleworks.info.dependency.MinecraftInfo;
import me.jonathing.gradleworks.info.dependency.MixinInfo;
import me.jonathing.gradleworks.info.publish.PublicationInfo;
import me.jonathing.gradleworks.info.workspace.JavaInfo;
import me.jonathing.gradleworks.info.workspace.ModInfo;
import me.jonathing.gradleworks.info.workspace.RunsInfo;
import me.jonathing.gradleworks.util.FileIOUtil;
import org.gradle.api.Project;
import org.gradle.api.provider.Property;

import java.io.File;

public class GradleWorksExtension
{
    private final Property<ModInfo> modInfo;
    private final Property<MinecraftInfo> mcInfo;
    private final Property<ForgeInfo> forgeInfo;
    private final Property<PublicationInfo> publicationInfo;

    private final Property<String> javaVersion;

    private final Property<MixinInfo> mixinInfo;

    private final Property<RunsInfo> runs;

    public GradleWorksExtension(Project project)
    {
        // initialize properties
        this.modInfo = project.getObjects().property(ModInfo.class);
        this.mcInfo = project.getObjects().property(MinecraftInfo.class);
        this.forgeInfo = project.getObjects().property(ForgeInfo.class);
        this.publicationInfo = project.getObjects().property(PublicationInfo.class);
        this.javaVersion = project.getObjects().property(String.class);
        this.mixinInfo = project.getObjects().property(MixinInfo.class);
        this.runs = project.getObjects().property(RunsInfo.class);

        // read the YAML files
        project.getLogger().info("Reading all YAML files for GradleWorks extension creation.");
        var dependenciesYAML = FileIOUtil.getYAMLData(new File(project.getProjectDir(), "dependencies.yaml"));
        var workspaceYAML = FileIOUtil.getYAMLData(new File(project.getProjectDir(), "workspace.yaml"));
        var publishingYAML = FileIOUtil.getYAMLData(new File(project.getProjectDir(), "publishing.yaml"));

        // set the properties
        this.modInfo.set(ModInfo.of(workspaceYAML.readData("mod")));
        this.mcInfo.set(MinecraftInfo.of(dependenciesYAML.readData("minecraft")));
        this.forgeInfo.set(ForgeInfo.of(dependenciesYAML.readData("minecraftforge")));
        this.publicationInfo.set(PublicationInfo.of(publishingYAML.readData("publication"), this.getModInfo(), this.getMcInfo().version()));
        this.javaVersion.set(JavaInfo.of(workspaceYAML.readData("java")).version());
        this.mixinInfo.set(MixinInfo.of(dependenciesYAML.readData("mixin")));
        this.runs.set(RunsInfo.of(workspaceYAML.readData("runs")));

        project.getLogger().info("GradleWorks extension created successfully.");
    }

    public ModInfo getModInfo()
    {
        return this.modInfo.get();
    }

    public MinecraftInfo getMcInfo()
    {
        return this.mcInfo.get();
    }

    public ForgeInfo getForgeInfo()
    {
        return this.forgeInfo.get();
    }

    public PublicationInfo getPublicationInfo()
    {
        return this.publicationInfo.get();
    }

    public boolean isPublishingEnabled()
    {
        return this.publicationInfo.get().isEnabled();
    }

    public String getJavaVersion()
    {
        return this.javaVersion.get();
    }

    public MixinInfo getMixinInfo()
    {
        return this.mixinInfo.get();
    }

    @Deprecated
    public boolean isMixinEnabled()
    {
        return this.mixinInfo.get().enabled();
    }

    public RunsInfo getRunsInfo()
    {
        return this.runs.get();
    }
}
