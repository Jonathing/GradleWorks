package me.jonathing.gradleworks.info.workspace;

import me.jonathing.gradleworks.reader.MappedDataReader;
import org.gradle.internal.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class RunsInfo implements Iterable<Pair<String, List<Pair<String, String>>>>
{
    List<Run> runs = new ArrayList<>();

    @SuppressWarnings("unchecked")
    public static RunsInfo of(MappedDataReader data)
    {
        RunsInfo runs = new RunsInfo();
        if (data != null)
        {
            data.getRawData().forEach((k, v) ->
            {
                Map<String, ?> properties = (Map<String, ?>) v;
                RunsInfo.Run run = new RunsInfo.Run(k);
                properties.forEach(run::property);
                runs.addRun(run);
            });
        }
        return runs;
    }

    @Override
    public Iterator<Pair<String, List<Pair<String, String>>>> iterator()
    {
        List<Pair<String, List<Pair<String, String>>>> list = new ArrayList<>();
        this.runs.forEach(run -> list.add(disassembleRun(run)));
        return list.stream().filter(run -> !run.getLeft().equals("all")).iterator();
    }

    public List<Pair<String, String>> getGlobalProperties()
    {
        return this.runs.stream().anyMatch(run -> run.name.equals("all")) ? this.runs.stream().filter(run -> run.name.equals("all")).collect(Collectors.toList()).get(0).properties : Collections.emptyList();
    }

    private static Pair<String, List<Pair<String, String>>> disassembleRun(Run run)
    {
        return Pair.of(run.name, run.properties);
    }

    protected void addRun(Run run)
    {
        if (this.contains(run.name))
            throw new IllegalArgumentException(String.format("Cannot overwrite an already existing run '%s'!", run.name));
        this.runs.add(run);
    }

    public boolean contains(String name)
    {
        return this.runs.stream().map(run -> run.name).anyMatch(s -> s.equals(name));
    }

    protected static class Run
    {
        private final String name;
        private final List<Pair<String, String>> properties;

        public Run(String name)
        {
            this.name = name;
            this.properties = new ArrayList<>();
        }

        public Run property(Object k, Object v)
        {
            return this.property(String.valueOf(k), String.valueOf(v));
        }

        public Run property(String k, String v)
        {
            this.properties.add(Pair.of(k, v));
            return this;
        }
    }
}
