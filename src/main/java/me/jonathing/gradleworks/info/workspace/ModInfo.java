package me.jonathing.gradleworks.info.workspace;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;

public record ModInfo(String modId, String group, String name, String author, String version)
{
    public static ModInfo of(IDataReader data)
    {
        try
        {
            var modId = data.readString("mod_id", true);
            var group = data.readString("group", true);
            var name = data.readString("name", true);
            var author = data.readString("author", true);
            var version = data.readString("version", true);
            return new ModInfo(modId, group, name, author, version);
        }
        catch (NullPointerException e)
        {
            throw new DataParseException("Mod configuration in 'workspace.yaml' is missing or incomplete!", e);
        }
    }

    public String archivesBaseName(String mcVersion)
    {
        return String.format("%s-%s", this.modId, mcVersion);
    }
}
