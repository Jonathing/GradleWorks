package me.jonathing.gradleworks.info.workspace;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;

public record JavaInfo(String version)
{
    public static JavaInfo of(IDataReader data)
    {
        try
        {
            var version = data.readString("version", true);
            return new JavaInfo(version);
        }
        catch (NullPointerException e)
        {
            throw new DataParseException("Java configuration in 'workspace.yaml' is missing or incomplete!", e);
        }
    }
}
