package me.jonathing.gradleworks.info.publish;

import me.jonathing.gradleworks.info.workspace.ModInfo;
import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;

import java.util.function.Supplier;

public class PublicationInfo
{
    private final boolean enabled;

    private final String group;
    private final String artifactId;
    private final String version;

    private final boolean javadoc;

    public static PublicationInfo of(IDataReader data, ModInfo modInfo, String mcVersion)
    {
        try
        {
            var enabled = data.readBoolean("enabled", true);
            var group = data.readString("group", true);
            var artifactId = data.readString("artifact_id", true);
            var version = data.readString("version", true);
            var javadoc = data.readBoolean("javadoc", false);
            return new PublicationInfo(modInfo, mcVersion, enabled, group, artifactId, version, javadoc);
        }
        catch (NullPointerException | ClassCastException e)
        {
            throw new DataParseException("Java configuration in 'workspace.yaml' is missing or incomplete!", e);
        }
    }

    public PublicationInfo(ModInfo modInfo, String mcVersion, boolean enabled, String group, String artifactId, String version, boolean javadoc)
    {
        this.enabled = enabled;
        this.group = getOrDefault(group, modInfo::group);
        this.artifactId = getOrDefault(artifactId, modInfo::modId);
        this.version = getOrDefault(version, () -> String.format("%s-%s", mcVersion, modInfo.version()));
        this.javadoc = javadoc;
    }

    private static String getOrDefault(String s, Supplier<String> defaultStr)
    {
        return !s.equalsIgnoreCase("DEFAULT") ? s : defaultStr.get();
    }

    public boolean isEnabled()
    {
        return this.enabled;
    }

    public String getGroup()
    {
        return this.group;
    }

    public String getArtifactId()
    {
        return this.artifactId;
    }

    public String getVersion()
    {
        return this.version;
    }

    public boolean shouldPublishJavadoc() {
        return javadoc;
    }
}
