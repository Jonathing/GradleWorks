package me.jonathing.gradleworks.info.publish;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.MappedDataReader;
import org.gradle.api.Project;
import org.gradle.internal.Pair;

public class RepositoryInfo
{
    private final String name;
    private final String url;
    private final Pair<String, String> username;
    private final Pair<String, String> password;

    public static RepositoryInfo of(Project project, MappedDataReader data)
    {
        String url = "INVALID";
        try
        {
            var name = data.readString("name", true);
            url = data.readString("url", true);

            if (data.containsKey("username"))
            {
                try
                {
                    var usernameSystem = data.readData("username").readString("property", true);
                    var usernameEnvVar = data.readData("username").readString("env", true);
                    var passwordSystem = data.readData("password").readString("property", true);
                    var passwordEnvVar = data.readData("password").readString("env", true);
                    return new RepositoryInfo(name, url, usernameSystem, usernameEnvVar, passwordSystem, passwordEnvVar);
                }
                catch (NullPointerException e)
                {
                    throw new DataParseException("The credentials (username/password) structure for the repository with URL %s is incomplete!".formatted(url), e);
                }
            }
            else
            {
                return new RepositoryInfo(project, name, url);
            }
        }
        catch (NullPointerException e)
        {
            throw new DataParseException("The repository information with URL %s is incomplete!".formatted(url), e);
        }
    }

    public RepositoryInfo(Project project, String name, String url)
    {
        this(project, name, url, null, null);
    }

    public RepositoryInfo(String name, String url, String usernameAsProperty, String usernameAsEnv, String passwordAsProperty, String passwordAsEnv)
    {
        this(null, name, url, Pair.of(usernameAsProperty, usernameAsEnv), Pair.of(passwordAsProperty, passwordAsEnv));
    }

    public RepositoryInfo(Project project, String name, String url, Pair<String, String> username, Pair<String, String> password)
    {
        this.name = name;
        this.url = project != null && url.equalsIgnoreCase("LOCAL") ? String.format("file:///%s/mcmodsrepo", project.getProjectDir()) : url;
        this.username = username;
        this.password = password;
    }

    public String getName()
    {
        return this.name;
    }

    public String getUrl()
    {
        return this.url;
    }

    public Pair<String, String> getUsernameVars()
    {
        return this.username;
    }

    public Pair<String, String> getPasswordVars()
    {
        return this.password;
    }

    public boolean requiresAuthentication()
    {
        return this.username != null;
    }
}
