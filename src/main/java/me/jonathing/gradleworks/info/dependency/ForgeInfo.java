package me.jonathing.gradleworks.info.dependency;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;

public record ForgeInfo(String toolchain, String version)
{
    public static ForgeInfo of(IDataReader data)
    {
        try
        {
            String toolchain = data.readString("toolchain", false);
            String version = data.readString("version", true);
            return new ForgeInfo(toolchain != null ? toolchain : "forge", version);
        }
        catch (NullPointerException e)
        {
            throw new DataParseException("Minecraft Forge dependency configuration in 'dependencies.yaml' is missing or incomplete!", e);
        }
    }
}
