package me.jonathing.gradleworks.info.dependency;

import me.jonathing.gradleworks.reader.IDataReader;
import org.jetbrains.annotations.Nullable;

public record JarJarInfo(boolean enabled, String ranged, String pin)
{
    @Nullable
    public static JarJarInfo of(IDataReader data)
    {
        if (data == null) return null;

        var enabled = data.readBoolean("enabled", true);
        var ranged = data.readString("ranged", true);
        var pin = data.readString("pin", true);
        return new JarJarInfo(enabled, ranged, pin);
    }
}
