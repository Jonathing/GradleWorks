package me.jonathing.gradleworks.info.dependency;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;
import org.jetbrains.annotations.Nullable;

public record MixinInfo(boolean enabled, String version, @Nullable String connector)
{
    public static MixinInfo of(IDataReader data)
    {
        try
        {
            var enabled = data.readBoolean("enabled", true);
            var version = data.readString("version", true);
            var connector = data.readString("connector", false);
            return new MixinInfo(enabled, version, connector);
        }
        catch (NullPointerException | ClassCastException e)
        {
            throw new DataParseException("Mixin dependency configuration in 'dependencies.yaml' is missing or incomplete!", e);
        }
    }
}
