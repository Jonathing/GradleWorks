package me.jonathing.gradleworks.info.dependency;

import me.jonathing.gradleworks.reader.DataParseException;
import me.jonathing.gradleworks.reader.IDataReader;

public record MinecraftInfo(String version, String mappingsChannel, String mappingsVersion)
{
    public static MinecraftInfo of(IDataReader data)
    {
        try
        {
            String version = data.readString("version", true);
            String mappingsChannel = data.readString("mappings_channel", true);
            String mappingsVersion = data.readString("mappings_version", true);
            return new MinecraftInfo(version, mappingsChannel, mappingsVersion);
        }
        catch (NullPointerException e)
        {
            throw new DataParseException("Minecraft dependency configuration in 'dependencies.yaml' is missing or incomplete!", e);
        }
    }
}
