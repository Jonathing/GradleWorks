package me.jonathing.gradleworks.dependency;

public class JarJarDependency extends ModDependency
{
    private final String rangedVersion;
    private final String pinnedVersion;

    public JarJarDependency(ModDependency orig, String rangedVersion, String pinnedVersion)
    {
        super(orig.enabled, orig.type.getName(), orig.deobf, orig.location, orig.mavenURL, orig.transitive);
        this.rangedVersion = rangedVersion;
        this.pinnedVersion = pinnedVersion;
    }

    @Override
    public String getType()
    {
        return "jarJar," + this.transitive + "," + this.pinnedVersion + "," + this.rangedVersion;
    }
}
