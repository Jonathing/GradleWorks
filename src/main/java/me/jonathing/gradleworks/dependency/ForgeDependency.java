package me.jonathing.gradleworks.dependency;

import me.jonathing.gradleworks.info.dependency.ForgeInfo;
import me.jonathing.gradleworks.info.dependency.MinecraftInfo;

public final class ForgeDependency extends ModDependency
{
    public ForgeDependency(MinecraftInfo minecraft, ForgeInfo forge)
    {
        super(true, Type.BOTH.getName(), false, makeLocation(minecraft, forge), null);
    }

    private static Location makeLocation(MinecraftInfo minecraft, ForgeInfo forge)
    {
        return new Location(String.format(
                "net.minecraftforge:%s:%s-%s",
                forge.toolchain(),
                minecraft.version(),
                forge.version()
        ));
    }

    @Override
    public String getType()
    {
        return "minecraft";
    }
}
