package me.jonathing.gradleworks.dependency;

import me.jonathing.gradleworks.reader.MappedDataReader;
import me.jonathing.gradleworks.util.AssertUtil;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

public class ModDependency
{
    final boolean enabled;
    final Type type;
    final boolean deobf;
    final Location location;
    final String mavenURL;
    final String transitive;

    @Deprecated
    public ModDependency(boolean enabled, String type, boolean deobf, Location location, String mavenURL)
    {
        this(enabled, type, deobf, location, mavenURL, "default");
    }

    public ModDependency(boolean enabled, String type, boolean deobf, Location location, String mavenURL, String transitive)
    {
        this.enabled = enabled;
        this.type = Type.of(type);
        this.deobf = deobf;
        this.location = location;
        this.mavenURL = mavenURL;
        this.transitive = transitive;
    }

    public boolean isEnabled()
    {
        return this.enabled;
    }

    @SuppressWarnings("unused")
    public String getType()
    {
        return this.type.toString() + "," + transitive;
    }

    @SuppressWarnings("unused")
    public boolean isDeobf()
    {
        return this.deobf;
    }

    public Location getLocation()
    {
        return this.location;
    }

    public String getMavenURL()
    {
        return this.mavenURL;
    }

    public static class Location
    {
        private final String location;
        private boolean curseforge = false;

        public Location(String location)
        {
            this.location = AssertUtil.stringRequireNonNull(location, "location", true);
        }

        public Location(String group, String name, String version, @Nullable String classifier)
        {
            if (classifier != null && classifier.equals("null"))
            {
                classifier = null;
            }

            AssertUtil.stringRequireNonNull(group, "group", true);
            AssertUtil.stringRequireNonNull(name, "name", true);
            AssertUtil.stringRequireNonNull(version, "version", true);

            StringBuilder builder = new StringBuilder(
                    2 + group.length() + name.length() + version.length() + (classifier != null ? classifier.length() + 1 : 0)
            );

            builder.append(group).append(':').append(name).append(':').append(version);
            if (classifier != null)
                builder.append(':').append(classifier);

            this.location = builder.toString();
        }

        public Location(String name, String project, String file)
        {
            this(String.format(
                    "curse.maven:%s-%s:%s",
                    AssertUtil.stringRequireNonNull(name, "name", true),
                    AssertUtil.stringRequireNonNull(project, "project", true),
                    AssertUtil.stringRequireNonNull(file, "file", true)
            ));
            this.curseforge = true;
        }

        public static Location invalid()
        {
            return new Location("INVALID");
        }

        @SuppressWarnings("unchecked")
        public static Location valueOf(Object obj)
        {
            if (obj instanceof String)
            {
                return new Location((String) obj);
            }

            if (obj instanceof Map)
            {
                var data = MappedDataReader.of(obj);

                if (data.containsKey("project"))
                {
                    return new Location(
                            data.readString("name", false),
                            data.readString("project", false),
                            data.readString("file", false)
                    );
                }
                else
                {
                    return new Location(
                            data.readString("group", false),
                            data.readString("name", false),
                            data.readString("version", false),
                            data.readString("classifier", false)
                    );
                }
            }

            return invalid();
        }

        public boolean isInvalid()
        {
            return this.location.equals("INVALID");
        }

        public boolean isCurseforge()
        {
            return this.curseforge;
        }

        @Override
        public String toString()
        {
            return this.location;
        }
    }

    public enum Type
    {
        RUNTIME("runtime", "runtimeOnly"),
        COMPILE("compile", "compileOnly"),
        BOTH("both", "implementation"),
        LIBRARY("library", "library"),
        ANNOTATION_PROCESSOR("annotation_processor", "annotationProcessor");

        private final String name;
        private final String group;

        Type(String name, String group)
        {
            this.name = name;
            this.group = group;
        }

        private static Type of(String name)
        {
            for (Type type : Type.values())
            {
                if (name.equalsIgnoreCase(type.getName()))
                    return type;
            }

            throw new IllegalArgumentException("Found an invalid dependency type: " + name);
        }

        public String getName()
        {
            return this.name;
        }

        @Override
        public String toString()
        {
            return this.group;
        }
    }

    private enum Transitive
    {
        DEFAULT("default"), TRUE("true"), FALSE("false");

        private final String name;

        Transitive(String name)
        {
            this.name = name;
        }
    }
}
