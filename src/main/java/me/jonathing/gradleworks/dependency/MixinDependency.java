package me.jonathing.gradleworks.dependency;

import me.jonathing.gradleworks.info.dependency.MixinInfo;

public final class MixinDependency extends ModDependency
{
    public MixinDependency(MixinInfo mixin)
    {
        super(mixin.enabled(), Type.ANNOTATION_PROCESSOR.getName(), false, makeLocation(mixin), null);
    }

    @Deprecated
    private MixinDependency(boolean enabled, String type, boolean deobf, Location location, String mavenURL)
    {
        super(enabled, type, deobf, location, mavenURL);
    }

    private static Location makeLocation(MixinInfo mixin)
    {
        return new Location(String.format("org.spongepowered:mixin:%s:processor", mixin.version()));
    }
}
