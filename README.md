# GradleWorks

Welcome to the GitHub repository for GradleWorks! GradleWorks is an attempt to make your life using ForgeGradle much, much easier, by automating the entirety of `build.gradle` and even adding some very nice features to it.

Want to use GradleWorks in your project or want to know more about how it works? I wrote a small [wiki](https://gitlab.com/Jonathing/GradleWorks/-/wikis/) that might probably have everything you'll need to know.
